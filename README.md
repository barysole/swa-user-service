# SWA - Semestral work - UserService

Author: Oleg Baryshnikov


## Description
This application works as one of microservices from the root project - https://gitlab.fel.cvut.cz/barysole/swa-root .
Current user catalog service provides functions to create, retrieve and delete users. After application start, the service tries to register themself to the eureka server which has port "8236". After that microservice listens to requests at endpoints described at (where?). Also the app connected with their own postgreSQL database (described more precisely in the root project).

## Documentation
Api documentation will be available at http://localhost:8236/swagger-ui/index.html after the app starts.
## GitLab pipeline
Project contains their own GitLab pipeline described in .gitlab-ci.yml. Gitlab pipeline contains three stages: build, test and building image.

Stage description:
- On a build stage the runner builds jar artefacts and stores them for further uses.
- On a test stage the runner runs an application unit-test.
- On a building stage runner build docker-image and push it to following private repository: https://hub.docker.com/repository/docker/verminaardo/swa-baryshnikov-user-service
## Most important app properties
```
spring.application.name=SwaUserService
server.port=8236
```