package ServiceTest;

import barysole.SwaUserServiceApplication;
import barysole.entity.User;
import barysole.exceptions.NotFoundException;
import barysole.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SwaUserServiceApplication.class)
public class UserServiceTests {

    @Autowired
    private UserService userService;

    private static User createTestUser() {
        User testUser = new User();
        testUser.setLogin("SupaUser322");
        testUser.setPassword("62c53048-ca7a-4ce4-bec6-e3e912c89d5a");
        testUser.setFirstName("Estella");
        testUser.setMiddleName("Kirkland");
        testUser.setEmail("estellakirkland@motovate.com");
        testUser.setTelephone("+4 (988) 596-2881");
        return testUser;
    }

    @Test
    public void create_and_get_user_should_execute_successfully() {
        User newUser = createTestUser();
        newUser = userService.saveUser(newUser);
        Assertions.assertEquals(newUser, userService.getUserById(newUser.getId()));
    }

    @Test
    public void get_nonexistent_user_should_throw_not_found_exception() {
        Assertions.assertThrows(NotFoundException.class, () -> userService.getUserById("123d"));
    }

    @Test
    public void delete_existent_user_should_execute_successfully() {
        User newUser = userService.saveUser(createTestUser());
        userService.deleteUserById(newUser.getId());
        Assertions.assertThrows(NotFoundException.class, () -> userService.getUserById(newUser.getId()));
    }

}
