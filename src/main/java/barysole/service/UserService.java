package barysole.service;

import barysole.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    Page<User> getAllUser(Pageable pageable);

    User getUserById(String id);

    User saveUser(User book);

    void deleteUserById(String id);

}
