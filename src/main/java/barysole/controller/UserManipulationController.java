package barysole.controller;

import barysole.dto.UserRequestDTO;
import barysole.dto.UserResponseDTO;
import barysole.entity.User;
import barysole.exceptions.NotFoundException;
import barysole.service.UserService;
import barysole.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserManipulationController extends GenericController {

    private final UserService userService;

    public UserManipulationController(UserService userService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.userService = userService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Users successfully retrieved"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<UserResponseDTO> getUserList(@PageableDefault(size = 1000) Pageable pageable) {
        return transformPage(userService.getAllUser(pageable), pageable, UserResponseDTO.class);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully retrieved"),
            @ApiResponse(responseCode = "404", description = "User not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public UserResponseDTO getUser(@PathVariable String id) {
        try {
            return transform(userService.getUserById(id), UserResponseDTO.class);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public UserResponseDTO saveUser(@RequestBody UserRequestDTO user) {
        return transform(userService.saveUser(transform(user, User.class)), UserResponseDTO.class);
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Users successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public List<UserResponseDTO> saveAllUsers(@RequestBody List<UserRequestDTO> userList) {
        List<UserResponseDTO> returnList = new ArrayList<>(userList.size());
        for (UserRequestDTO user :
                userList) {
            returnList.add(transform(userService.saveUser(transform(user, User.class)), UserResponseDTO.class));
        }
        return returnList;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully deleted", content = @Content()),
            @ApiResponse(responseCode = "404", description = "User not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public void deleteUser(@PathVariable String id) {
        try {
            userService.deleteUserById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
