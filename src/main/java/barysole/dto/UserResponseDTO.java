package barysole.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Column;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponseDTO {

    private String id;

    private String login;

    private String fio;

    private String firstName;

    private String lastName;

    private String middleName;

    private String email;

    private String telephone;

}
