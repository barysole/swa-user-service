package barysole.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRequestDTO {

    @Schema(hidden = true)
    private String id;

    private String login;

    private String fio;

    private String firstName;

    private String lastName;

    private String middleName;

    private String email;

    private String telephone;

    private String password;

}