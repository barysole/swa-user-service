package barysole.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.StringJoiner;

@Entity
@Table(name = "Users")
@Data
@ToString(of = {"login"})
@EqualsAndHashCode(of = {"login", "email"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @Column(name = "login", unique = true, nullable = false)
    private String login;

    @Column(name = "password", nullable = false, length = 64)
    public String password;

    @Transient
    public String getFio() {
        StringJoiner joiner = new StringJoiner(" ");
        if (lastName != null) {
            joiner.add(lastName);
        }
        if (firstName != null) {
            joiner.add(firstName);
        }
        if (middleName != null) {
            joiner.add(middleName);
        }
        return joiner.toString();
    }

    @Column(name = "firstName", nullable = false)
    private String firstName;

    @Column(name = "middleName", nullable = false)
    private String middleName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "telephone")
    private String telephone;

}
